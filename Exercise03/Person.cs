﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise03
{
    public class Person
    {
        private string cpr;
        private DateTime birthDay;

        public Person(string cpr)
        {
            CPR = cpr;

            try
            {
                birthDay = new DateTime(getYear(), int.Parse(CPR.Substring(2, 2)), int.Parse(CPR.Substring(0, 2)));
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentException("First 6 digits of CPR number is not a valid date.");
            }
        }

        public string CPR
        {
            get { return cpr; }
            set {
                if (!(value.Length == 10))
                {
                    throw new ArgumentException("CPR number is not 10 digits long.");
                }
                foreach (char c in value)
                {
                    if (!char.IsDigit(c))
                    {
                        throw new ArgumentException("Non-digit in CPR number");
                    }
                }
                cpr = value; }
        }

        public DateTime BirthDay
        {
            get { return birthDay; }
        }

        // extract the year in 4 digits from cpr
        // if cpr[6]<’5’ the year is 1900-1999, if cpr[6]>=’5’ the year is 2000..2099
        private int getYear()
        {
            if (cpr[6] < '5')
            {
                return 1900 + int.Parse(cpr.Substring(4, 2)); 
            }
            else 
            {
                return 2000 + int.Parse(cpr.Substring(4, 2));
            }
        }


    }
}
