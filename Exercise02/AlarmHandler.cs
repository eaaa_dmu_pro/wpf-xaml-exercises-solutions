﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    interface AlarmHandler
    {
        void StatusChangedEventHandler(Status status);
        void FiveSecondsIntervalEventHandler(DateTime time);
        void AlarmSentEventHandler(DateTime time, string description);
    }
}
