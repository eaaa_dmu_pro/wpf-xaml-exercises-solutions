﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Exercise02
{
    /// <summary>
    /// Interaction logic for Alarms.xaml
    /// </summary>
    public partial class Alarms : Window, AlarmHandler
    {
        public Alarms()
        {
            InitializeComponent();
        }

        public Alarms(AlarmEvents alarmEvents)
        {
            InitializeComponent();
            alarmEvents.AlarmEvent += AlarmSentEventHandler;
            alarmEvents.FiveSecondsIntervalEvent += FiveSecondsIntervalEventHandler;
            alarmEvents.StatusChangedEvent += StatusChangedEventHandler;
        }

        public void StatusChangedEventHandler(Status status)
        {
            StatusLabel.Content = status;
        }

        public void FiveSecondsIntervalEventHandler(DateTime time)
        {
            AliveLabel.Content = time.ToLongTimeString();
        }

        public void AlarmSentEventHandler(DateTime time, string description)
        {
            AlarmsRecieved.Items.Add(time.ToLongTimeString() + ": " + description);
        }
    }
}
