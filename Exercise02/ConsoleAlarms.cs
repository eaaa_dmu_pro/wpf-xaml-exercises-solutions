﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    public class ConsoleAlarms : AlarmHandler
    {

        public ConsoleAlarms(AlarmEvents alarmEvents)
        {
            alarmEvents.FiveSecondsIntervalEvent += FiveSecondsIntervalEventHandler;
            alarmEvents.StatusChangedEvent += StatusChangedEventHandler;
            alarmEvents.AlarmEvent += AlarmSentEventHandler;
        }
        public void AlarmSentEventHandler(DateTime time, string description)
        {
            Console.WriteLine($"{time.ToLongTimeString()} : {description}");
        }

        public void FiveSecondsIntervalEventHandler(DateTime time)
        {
            Console.WriteLine($"Ping modtaget kl. {time.ToLongTimeString()} ");
        }

        public void StatusChangedEventHandler(Status status)
        {
            Console.WriteLine($"Ny status: {status.ToString()}");
        }
    }
}
