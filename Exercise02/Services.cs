﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    public enum Status { STOPPED, STANDBY, ACTIVE }

    public class AlarmEvents
    {
        public delegate void StatusChangedEventHandler(Status status);
        public delegate void FiveSecondsIntervalEventHandler(DateTime time);
        public delegate void AlarmSentEventHandler(DateTime time, string description);

        public event StatusChangedEventHandler StatusChangedEvent;

        public void OnStatusChangedEvent(Status status)
        {
            StatusChangedEvent?.Invoke(status);
        }

        public event FiveSecondsIntervalEventHandler FiveSecondsIntervalEvent;

        public void OnFiveSecondsIntervalEvent(object sender, EventArgs e)
        {
            FiveSecondsIntervalEvent?.Invoke(DateTime.Now);
        }

        public event AlarmSentEventHandler AlarmEvent;

        public void OnAlarmSentEvent(String alarmText)
        {
            AlarmEvent?.Invoke(DateTime.Now, alarmText);
        }
    }

}
