﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Exercise02
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public AlarmEvents alarmEvents;

        public MainWindow()
        {
            alarmEvents = new AlarmEvents();
            Alarms alarms = new Alarms(alarmEvents);
            Alarms alarms2 = new Alarms(alarmEvents);
            ConsoleAlarms alarm3 = new ConsoleAlarms(alarmEvents);

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(5);
            timer.Tick += alarmEvents.OnFiveSecondsIntervalEvent;

            InitializeComponent();

            alarms.Show();
            alarms2.Show();

            timer.Start();
        }

        private void StoppedRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            alarmEvents.OnStatusChangedEvent(Status.STOPPED);
        }

        private void StandbyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            alarmEvents.OnStatusChangedEvent(Status.STANDBY);
        }

        private void ActiveRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            alarmEvents.OnStatusChangedEvent(Status.ACTIVE);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            alarmEvents.OnAlarmSentEvent(AlarmText.Text);
            AlarmText.Text = "";
        }
    }
}
