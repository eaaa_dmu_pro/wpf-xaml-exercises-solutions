﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace FractalTree
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DrawLine(Point p1, Point p2, int thickness)
        {
            Line line = new Line();
            line.X1 = p1.X;
            line.X2 = p2.X;
            line.Y1 = p1.Y;
            line.Y2 = p2.Y;
            line.Stroke = Brushes.RosyBrown;
            line.StrokeThickness = thickness;
            myCanvas.Children.Add(line);
        }

        private void DrawTree(double x0, double y0, double length, double angle)
        {
            Point p1 = new Point { X = x0, Y = y0 };
            Point p2 = new Point();
            p2.X = x0 + length * Math.Cos(Math.PI / 180 * angle);
            p2.Y = y0 + length * Math.Sin(Math.PI / 180 * angle);
            DrawLine(p1, p2, (int)Math.Round(length * 0.3));
            if (length > 3)
            {
                DrawTree(p2.X, p2.Y, length * 0.75, angle - 30);
                DrawTree(p2.X, p2.Y, length * 0.68, angle + 50);
            }
            else
            {
                DrawLeaf(p2);
            }
        }

        private void DrawLeaf(Point point)
        {
            var leaf = new Ellipse();
            leaf.Stroke = Brushes.Green;
            leaf.Fill = Brushes.Green;
            leaf.Width = 3;
            leaf.Height = 4;
            leaf.Visibility = Visibility.Visible;
            Canvas.SetLeft(leaf, point.X);
            Canvas.SetTop(leaf, point.Y);
            myCanvas.Children.Add(leaf);
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DrawTree(300, 350, 100, -90);
        }
    }
}
