﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise03;

namespace Exercise03UnitTests
{
    [TestClass]
    public class UnitTestPerson
    {
        [TestMethod]
        public void TestConstructor()
        {
            Person person = new Person("0509724321");
            Assert.AreEqual("0509724321", person.CPR, "Cpr number does not match");
            Assert.AreEqual(new DateTime(1972, 9, 5), person.BirthDay);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void TestCprNot10DigitsLong()
        {
            Person testPerson = new Person("123");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void TestCprContainsNonDigit()
        {
            Person testPerson = new Person("123456789a");
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void TestCprNonValidDate()
        {
            Person testPerson = new Person("9988776655");
        }

        [TestMethod]
        public void Test20Century()
        {
            Person person = new Person("0101724000");
            Assert.AreEqual(1972, person.BirthDay.Year);
        }

        [TestMethod]
        public void Test21Centory()
        {
            Person person = new Person("0101106000");
            Assert.AreEqual(2010, person.BirthDay.Year);
        }

    }
}
